/* global malarkey:false, moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';

angular.module('automatoLanding', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router']).constant('malarkey', malarkey).constant('moment', moment).config(config).config(routerConfig).run(runBlock).controller('MainController', MainController);

//# sourceMappingURL=index.module-compiled.js.map

//# sourceMappingURL=index.module-compiled-compiled.js.map

//# sourceMappingURL=index.module-compiled-compiled-compiled.js.map