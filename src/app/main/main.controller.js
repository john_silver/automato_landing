export class MainController {
  constructor ($location, $anchorScroll, anchorSmoothScroll, $http) {
    'ngInject';


    var vm = this;

    vm.works = [
      {'img':'assets/images/iMac.png', 'title': 'Аналитика', 'sub': 'Управляйте компанией легко'},
      {'img':'assets/images/iMac.png', 'title': 'Все контакты как на ладони', 'sub': 'Ведите историю каждого клиента и знайте в любой момент времени, что клиент заказывал у Вас месяц, а может год назад. Cформируйте его привычки.'},
      {'img':'assets/images/iMac.png', 'title': 'ЛЮБАЯ ПРОЦЕДУРА - это сделка', 'sub': 'Теперь вы точно знаете, сколько прибыли приносит Вам каждый клиент.'},
      {'img':'assets/images/iMac.png', 'title': 'Оставляйте приятное впечатление', 'sub': 'Процедура успешно завершена? Отправьте смс автоматически клиенту с благодарностью. Он или она будет приятно удивлена Вашим вниманием.'},
      {'img':'assets/images/iMac.png', 'title': 'напоминайте о себе в два клика', 'sub': 'Отправляйте клиентам информацию об акции. Они должны это знать.'}
    ];

    vm.clients = [
      {'img':'assets/images/clients/client1.png'},
      {'img':'assets/images/clients/client2.png'},
      {'img':'assets/images/clients/client3.png'},
      {'img':'assets/images/clients/client4.png'},
      {'img':'assets/images/clients/client5.png'},
      {'img':'assets/images/clients/client6.png'},
      {'img':'assets/images/clients/client7.png'},
      {'img':'assets/images/clients/client8.png'},
      {'img':'assets/images/clients/client9.png'},
      {'img':'assets/images/clients/client10.png'},
      {'img':'assets/images/clients/client11.png'},
      {'img':'assets/images/clients/client12.png'},
      {'img':'assets/images/clients/client13.png'},
      {'img':'assets/images/clients/client14.png'},
      {'img':'assets/images/clients/client16.png'},
      {'img':'assets/images/clients/client17.png'}
    ];

    vm.currentIndex = 0;

    vm.setCurrentSlideIndex = function (index) {
      vm.currentIndex = index;
    };

    vm.isCurrentSlideIndex = function (index) {
      return vm.currentIndex === index;
    };

    vm.prev = function () {
      vm.currentIndex = (vm.currentIndex < vm.works.length - 1) ? ++vm.currentIndex : 0;
    };

    vm.next = function () {
      vm.currentIndex = (vm.currentIndex > 0) ? --vm.currentIndex : vm.works.length - 1;
    };
    vm.scroll = function(){
      var newHash = 'more';
      //if ($location.hash() !== newHash) {
      //  $location.hash('more');
      //} else {
      //  anchorSmoothScroll('more');
      //}
      anchorSmoothScroll.scrollTo('more');
    }


      $http.get('http://app.automato.me/api/adminpanel/count')
        .success(function(data){
          vm.count = data.count;
        })
        .error(function(err){
          vm.count = 0;
        });
  }




}
