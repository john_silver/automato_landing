/* global malarkey:false, moment:false */

import { config } from './index.config';
import { routerConfig } from './index.route';
import { runBlock } from './index.run';
import { MainController } from './main/main.controller';
import { anchorSmoothScroll } from './components/anchorSmoothScroll/anchorSmoothScroll.service.js';

console.log(anchorSmoothScroll);
angular.module('automatoLanding', ['ngAnimate', 'ngCookies', 'ngTouch', 'ngSanitize', 'ngMessages', 'ngAria', 'ngResource', 'ui.router'])
  .constant('malarkey', malarkey)
  .constant('moment', moment)
  .config(config)
  .config(routerConfig)
  .run(runBlock)
  .service('anchorSmoothScroll', anchorSmoothScroll)
  .controller('MainController', MainController);
